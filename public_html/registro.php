<!DOCTYPE html>
<html>
    <head>
       
       <!-- <meta http-equiv="Content-Security-Policy" content="default-src 'self' data: gap: https://ssl.gstatic.com 'unsafe-eval'; style-src 'self' 'unsafe-inline'; media-src *">-->
	   <!--la linea anterior corresponde a seguridad, que por ahora, no usaremos y nos pone problemas a la hora de ejecutar funciones-->
	    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
           
		
		
		 	 <title>Registrarse</title>
		 
		<link rel="stylesheet" type="text/css" href="css/registro.css">
		
		       
    </head>
    <body>
		
<div id="formreg">
<div id="title"><h1>REGISTRO</h1></div>
<form action="registro2.php" method="post" id="formreg" accept-charset="UTF-8" name="formreg" >
		<!-- usuario-->
		<div id="usuario">
    			<label id="luser" class="ldata">Nombre de Usuario</label><br>
    			<input name="user" id="user" class="dato" type= "text" placeholder="Nombre de Usuario" >
        	</div><br>
		<!-- fin usuario-->

		<!-- nombre-->
		<div id="divnombre">
    			<label id="lnom" class="ldata">Nombre</label><br>
    			<input name="nombre" id="nombre" class="dato" type= "text" placeholder="Nombre">
        	</div><br>
		<!-- fin nombre-->

		<!-- apellido paterno-->
		<div id="apellidopaterno">
    			<label id="apellido-paterno" class="ldata">Apellido Paterno</label><br>
    			<input name="inpaterno" id="inpaterno" class="dato" type= "text" placeholder="Apellido Paterno">
        	</div><br>
		<!-- fin apellido materno-->

		<!-- apellido materno-->
		<div id="apellidomaterno">
    			<label id="apellido-materno" class="ldata">Apellido Materno</label><br>
    			<input name="inmaterno" id="inmaterno" class="dato" type= "text" placeholder="Apellido Materno">
        	</div><br>
		<!-- fin apellido materno-->


		<!-- sexo-->
		<div id="sexo">
    			<label id="lsexo" class="ldata">Sexo</label><br>
    			<input type="radio"  name="sexo" value="M"><label class="lradio">Hombre</label>
 				<input type="radio" name="sexo" value="F"><label class="lradio">Mujer</label></div><br>

		<!-- fin sexo-->

		<!--fecha-->
		<div id="fecha">
    			<label id="lfecha" class="ldata">Fecha de Nacimiento</label><br>
			<input name="ifecha" id="ifecha" class="dato" type="date" placeholder="dd-mm-aa">
    		</div><br>
		<!-- fin fecha-->

		<!-- email-->
		<div id="email">
    			<label id="lmail" class="ldata">Email</label><br>
    			<input name="mail" id="mail" class="dato" type= "email" placeholder="example@123.com">
        	</div><br>
		<!-- fin email-->

		<!-- celular-->
		<span><div id="celular">
    			<label id="lcell" class="ldata">Celular</label><br>
    			<input name="incel" id="incel" class="dato" type= "text" placeholder="Celular">
        	</div><br>
		<!-- fin celular-->

		<!--contraseña-->
    		<div id="clave">
    			<label for="contrasena" id="lpass" class="ldata">Contraseña</label><br>
    			<input name="pass" id="pass" class="dato" placeholder="Contraseña" type="password">
  		</div><br>
		<div id="rclave">
    			<label for="rcontrasena" id="lrpass" class="ldata">Repetir Contraseña</label><br>
    			<input name="rpass" id="rpass" class="dato" placeholder="Contraseña" type="password">
  		</div><br>

		<!--contraseña-->


<input type="button" id="registro" class="bton" value="Registarse" /><br><br>
</form>

<button class="bton" onclick="location.href='index.html'" id = "perfil usuario">Volver</button>
		</div>
		
		
    </body>
</html>
