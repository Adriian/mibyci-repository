$(document).ready(function(){
	var nombreUsuario=window.localStorage.getItem('nombreusuario');
	var objeto ={nombre:nombreUsuario};
	$.ajax({
		url:"http://afrodita.inf.ucv.cl/~bbuglioni/lista_bici_celular.php",
		type:"POST",
		data:objeto,
		dataType:"json",
		success:function(result){
			var elem="";
			for (i = 0; i < result.length;i++){
				$('#lista_bicicletas2').append(result[i]);
        	}
		}
	})
});


function mostrar_datos(){
	window.localStorage.setItem("Marca_mostrar","");
	window.localStorage.setItem("Modelo_mostrar","");
	window.localStorage.setItem("Aro_mostrar","");
	window.localStorage.setItem("Nombre_dueno_mostrar","");
	window.localStorage.setItem("Color_mostrar","");
	window.localStorage.setItem("Estado_mostrar","");
	var id_bici_delete=document.getElementById('lista_bicicletas2').value;
	var objeto ={opcion:"mostrar datos",id_bici:id_bici_delete};
	$.ajax({
		url:"http://afrodita.inf.ucv.cl/~bbuglioni/eliminar_bicicleta_celu.php",
		type:"POST",
		data:objeto,
		dataType:"json",
		success:function(result){
			//se guardan los valores recibidos en la consulta
				window.localStorage.setItem("Marca_mostrar",result['Marca']);
				window.localStorage.setItem("Modelo_mostrar",result['Modelo']);
				window.localStorage.setItem("Aro_mostrar",result['Aro']);
				window.localStorage.setItem("Nombre_mostrar",result['Id_usuario']);
				window.localStorage.setItem("Color_mostrar",result['Color']);
				window.localStorage.setItem("Estado_mostrar",result['Estado']);
			//Se setean los nuevos valores antes de hacer visible los campos

			//----------Numero de serie--------------
			var id_bici=document.getElementById('lista_bicicletas2').value;
            document.getElementById('numero').innerHTML=id_bici;

            //----------Modelo-----------------------
            var modelo_bici=window.localStorage.getItem('Modelo_mostrar');
            document.getElementById('modelo').innerHTML=modelo_bici;

            //----------Marca------------------------
			var marca_bici=window.localStorage.getItem('Marca_mostrar');
            document.getElementById('label-marca').innerHTML=marca_bici;

            //----------Aro--------------------------
            var aro_bici=window.localStorage.getItem('Aro_mostrar');
            document.getElementById('aro').innerHTML=aro_bici;

            //----------Color-----------------------
            var color_bici=window.localStorage.getItem('Color_mostrar');
            document.getElementById('color').innerHTML=color_bici;
            
            //----------Estado--------------------
            var estado_bici=window.localStorage.getItem('Estado_mostrar');
            if(estado_bici=="0"){
              document.getElementById('estado').innerHTML="Bicicleta NO PERDIDA";
            }else{
              document.getElementById('estado').innerHTML="Bicicleta PERDIDA";
            }


			//se vuelve visible todo lo que estaba oculto (datos de la bicicleta a eliminar)
				document.getElementById('numeroserie').style.visibility='visible';
    			document.getElementById('divmodelo').style.visibility='visible';
    			document.getElementById('divmarca').style.visibility='visible';
    			document.getElementById('divaro').style.visibility='visible';
  				document.getElementById('divcolor').style.visibility='visible';
			    document.getElementById('estado').style.visibility='visible';
		}
	})
}

function delete_bike(){
	var r = confirm("La bicicleta seleccionada se eliminara PERMANENTEMENTE, desea continuar?");
    if (r == true) {
    	var id_bici_delete=document.getElementById('lista_bicicletas2').value;
    	var objeto={opcion:"delete",id_bici:id_bici_delete};
    	$.ajax({
		url:"http://afrodita.inf.ucv.cl/~bbuglioni/eliminar_bicicleta_celu.php",
		type:"POST",
		data:objeto,
		dataType:"json",
		success:function(result){
			if(result=="true"){
				alert("La bicicleta fue eliminada con exito!");
				window.location="perfil.html";
			}else{
				alert("Ocurrio un problema, intentelo nuevamente");
			}
			
		}
	})

        
    }
}