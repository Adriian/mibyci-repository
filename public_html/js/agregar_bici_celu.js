
$("#agregar_bici").on("click",function(){
		var numero_serie = $("#numero_serie").val(); //#significa id
		var modelo = $("#modelo").val();
		var marca = $("#marca").val();
		var aro = $("#aro").val();
		var color = $('#color').val();
		var eleccion = "agregar bici";
		var nombre_usuario= window.localStorage.getItem("nombreusuario");
		if(nombre_usuario=="null"){
			alert("Debe estar logeado para realizar esta accion");
			window.location="logincelu.html";
		}
		
		if ( (numero_serie == "")||(modelo == "")||(marca == "")||(aro == "")||(color== "") )
			alert ("Es necesario rellenar todos los campos");
		else{
			var objeto = {opcion:eleccion,nro_ingresado:numero_serie,modelo_ingresado:modelo,marca_ingresada:marca,aro_ingresado:aro,color_ingresado:color,nombre_usuario_logeado:nombre_usuario}; //estos son los datos que se enviaran al servidor, al php
			$.ajax({
			url:"http://afrodita.inf.ucv.cl/~bbuglioni/agregar_bici_celu.php",  //los php son los unicos que estan en el servidor
			type:"POST",
			data:objeto,
			dataType:"json",
			success:function(result){
				alert(result);
				if(result=="Bicicleta REGISTRADA!"){
					window.localStorage.setItem("nombreusuario",nombre_usuario);
					window.location="perfil.html";
				}
			}
			});
		}
	});
	